class User {
  User({
    required this.code,
    required this.movies,
  });

  final int code;
  final List<Movie> movies;
}

class Movie {
  Movie({
    required this.id,
    required this.title,
    required this.description,
    required this.rating,
    required this.year,
    required this.category,
    required this.img,
  });

  final String id;
  final String title;
  final String description;
  final String rating;
  final String year;
  final String category;
  final String img;
}
