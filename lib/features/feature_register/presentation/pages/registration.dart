import 'package:epic_movieapp/core/constants/constants.dart';
import 'package:epic_movieapp/core/injections/injection_container.dart';
import 'package:epic_movieapp/features/feature_login/presentation/pages/login.dart';
import 'package:epic_movieapp/features/feature_register/presentation/bloc/register_bloc.dart';
import 'package:epic_movieapp/features/feature_register/presentation/widgets/register_form_fields.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Registration extends StatefulWidget {
  const Registration({Key? key}) : super(key: key);

  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  final _formKey = GlobalKey<FormState>();
  final _bloc = sl<RegisterBloc>();

  Widget signUpButton() {
    return Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.pinkAccent,
      child: Builder(builder: (context) {
        return MaterialButton(
            padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
            minWidth: MediaQuery.of(context).size.width,
            onPressed: () {
              if (_formKey.currentState!.validate()) {
                BlocProvider.of<RegisterBloc>(context).add(
                  GetRegisterEvent(
                    reqCode: ApiCodes.regCode,
                    name: nameEditingController.text,
                    email: emailEditingController.text,
                    username: usernameEditingController.text,
                    password: passwordEditingController.text,
                  ),
                );
              }
            },
            child: const Text(
              "Create Account",
              textAlign: TextAlign.center,
              style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                  fontWeight: FontWeight.bold),
            ));
      }),
    );
  }

  Widget cancelButton() {
    return Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.transparent,
      child: MaterialButton(
          padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text(
            "Cancel",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          )),
    );
  }

  @override
  Widget build(BuildContext appContext) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: const Text(
          "Movie App",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'FrScript',
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 36.0,
          ),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: BlocProvider(
          create: (BuildContext appContext) => _bloc,
          child: Center(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: BlocConsumer<RegisterBloc, RegisterState>(
                  listener: (context, state) {
                    if (state is RegisterSuccessState) {
                      if (state.registerResponseEntity.code == 200) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Account Created Successfully!"),
                            backgroundColor: Colors.green,
                          ),
                        );
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => const Login()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(state.registerResponseEntity.msg),
                            backgroundColor: Colors.red,
                          ),
                        );
                      }
                    } else if (state is RegisterErrorState) {
                      ScaffoldMessenger.of(appContext).showSnackBar(
                        SnackBar(
                          content: Text(state.error.toString()),
                          backgroundColor: Colors.red,
                        ),
                      );
                    }
                  },
                  builder: (context, state) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.07),
                                  border: Border.all(color: Colors.black12),
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(40.0),
                                      topRight: Radius.circular(40.0),
                                      bottomLeft: Radius.circular(40.0),
                                      bottomRight: Radius.circular(40.0))),
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  children: [
                                    const Text(
                                      "Create Account",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: 'FrScript',
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 36.0,
                                      ),
                                    ),
                                    const SizedBox(height: 30),
                                    nameField(),
                                    const SizedBox(height: 20),
                                    emailField(),
                                    const SizedBox(height: 20),
                                    userNameField(),
                                    const SizedBox(height: 20),
                                    passwordField(),
                                    const SizedBox(height: 20),
                                    confirmPasswordField(),
                                    const SizedBox(height: 30),
                                    signUpButton(),
                                    SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                    ),
                                    Container(
                                      child: state is RegisterLoadingState
                                          ? const CircularProgressIndicator()
                                          : null,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.05,
                            ),
                            cancelButton(),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
