part of 'register_bloc.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();

}

class RegisterInitialState extends RegisterState {
  @override
  List<Object?> get props => [];
}

class RegisterLoadingState extends RegisterState {
  @override
  List<Object?> get props => [];
}

class RegisterSuccessState extends RegisterState {
  final GetRegisterResponseEntity registerResponseEntity;

  const RegisterSuccessState({required this.registerResponseEntity});
  @override
  List<Object?> get props => [];
}

class RegisterErrorState extends RegisterState {
  final String error;

  const RegisterErrorState({required this.error});
  @override
  List<Object?> get props => [];
}
