import 'package:flutter/material.dart';

final nameEditingController = TextEditingController();
final emailEditingController = TextEditingController();
final usernameEditingController = TextEditingController();
final passwordEditingController = TextEditingController();
final confirmPasswordEditingController = TextEditingController();

Widget nameField() {
  return TextFormField(
    autofocus: false,
    controller: nameEditingController,
    keyboardType: TextInputType.name,
    validator: (value) {
      RegExp regex = RegExp(r'^.{3,}$');
      if (value!.isEmpty) {
        return ("Name cannot be Empty");
      }
      if (!regex.hasMatch(value)) {
        return ("Enter Valid name(Min. 3 Character)");
      }
      return null;
    },
    onSaved: (value) {
      nameEditingController.text = value!;
    },
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.account_circle),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Name",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}

Widget emailField() {
  return TextFormField(
    autofocus: false,
    controller: emailEditingController,
    keyboardType: TextInputType.emailAddress,
    validator: (value) {
      if (value!.isEmpty) {
        return ("Please Enter Your Email");
      }
      // reg expression for email validation
      if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]").hasMatch(value)) {
        return ("Please Enter a valid email");
      }
      return null;
    },
    onSaved: (value) {
      emailEditingController.text = value!;
    },
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.mail),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Email",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}

Widget userNameField() {
  return TextFormField(
    autofocus: false,
    controller: usernameEditingController,
    keyboardType: TextInputType.name,
    validator: (value) {
      if (value!.isEmpty) {
        return ("Username cannot be Empty");
      }
      return null;
    },
    onSaved: (value) {
      usernameEditingController.text = value!;
    },
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.account_circle),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Userame",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}

Widget passwordField() {
  return TextFormField(
    autofocus: false,
    controller: passwordEditingController,
    obscureText: true,
    validator: (value) {
      RegExp regex = RegExp(r'^.{6,}$');
      if (value!.isEmpty) {
        return ("Password is required for login");
      }
      if (!regex.hasMatch(value)) {
        return ("Enter Valid Password(Min. 6 Character)");
      }
    },
    onSaved: (value) {
      passwordEditingController.text = value!;
    },
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.vpn_key),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Password",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}

Widget confirmPasswordField() {
  return TextFormField(
    autofocus: false,
    controller: confirmPasswordEditingController,
    obscureText: true,
    validator: (value) {
      if (confirmPasswordEditingController.text !=
          passwordEditingController.text) {
        return "Password don't match";
      }
      return null;
    },
    onSaved: (value) {
      confirmPasswordEditingController.text = value!;
    },
    textInputAction: TextInputAction.done,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.vpn_key),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Confirm Password",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}
