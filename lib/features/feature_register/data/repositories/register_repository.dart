import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/exception.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/core/network/network_info.dart';
import 'package:epic_movieapp/features/feature_register/data/data_sources/remote_data_source/remote_register_data_source.dart';
import 'package:epic_movieapp/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:epic_movieapp/features/feature_register/domain/entities/response/register_response_entity.dart';
import 'package:epic_movieapp/features/feature_register/domain/repositories/register_repository.dart';

class RegisterRepositoryImpl implements RegisterRepository {
  final NetworkInfo networkInfo;
  final RemoteRegisterDataSource remoteData;

  RegisterRepositoryImpl({required this.networkInfo, required this.remoteData});

  @override
  Future<Either<Failure, GetRegisterResponseEntity>> getRegisterRequestRepository(
      GetRegisterRequestEntity getRegisterRequestEntity) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(await remoteData.registerRequest(getRegisterRequestEntity));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
