import 'dart:convert';

GetRegisterRequest getRegisterRequestFromJson(String str) =>
    GetRegisterRequest.fromJson(json.decode(str));

String getRegisterRequestToJson(GetRegisterRequest data) =>
    json.encode(data.toJson());

class GetRegisterRequest {
  GetRegisterRequest({
    required this.reqCode,
    required this.name,
    required this.email,
    required this.username,
    required this.password,
  });

  final int reqCode;
  final String name;
  final String email;
  final String username;
  final String password;

  factory GetRegisterRequest.fromJson(Map<String, dynamic> json) =>
      GetRegisterRequest(
        reqCode: json["req_code"],
        name: json["name"],
        email: json["email"],
        username: json["username"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "req_code": reqCode,
        "name": name,
        "email": email,
        "username": username,
        "password": password,
      };
}
