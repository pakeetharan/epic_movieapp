import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/core/usecase/usecase.dart';
import 'package:epic_movieapp/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:epic_movieapp/features/feature_register/domain/entities/response/register_response_entity.dart';
import 'package:epic_movieapp/features/feature_register/domain/repositories/register_repository.dart';

class GetRegisterUseCase
    extends UseCase<GetRegisterResponseEntity, GetRegisterRequestEntity> {
  final RegisterRepository repository;

  GetRegisterUseCase({required this.repository});
  @override
  Future<Either<Failure, GetRegisterResponseEntity>> call(
      GetRegisterRequestEntity params) async {
    return await repository.getRegisterRequestRepository(params);
  }
}
