import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/features/feature_register/domain/entities/request/register_request_entity.dart';
import 'package:epic_movieapp/features/feature_register/domain/entities/response/register_response_entity.dart';

abstract class RegisterRepository {
  Future<Either<Failure, GetRegisterResponseEntity>> getRegisterRequestRepository(
      GetRegisterRequestEntity getRegisterRequestEntity);
}
