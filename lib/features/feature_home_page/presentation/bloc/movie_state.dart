part of 'movie_bloc.dart';

abstract class MovieState extends Equatable {
  const MovieState();
}

class MovieInitialState extends MovieState {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class MovieLoadingState extends MovieState {
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}

class MovieSuccessState extends MovieState {
  final GetMovieResponseEntity movieResponseEntity;

  const MovieSuccessState({required this.movieResponseEntity});
  @override
  List<Object?> get props => throw UnimplementedError();
}

class MovieErrorState extends MovieState {
  final String error;

  const MovieErrorState({required this.error});
  @override
  // TODO: implement props
  List<Object?> get props => throw UnimplementedError();
}
