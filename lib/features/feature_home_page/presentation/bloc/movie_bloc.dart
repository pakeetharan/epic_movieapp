import 'package:bloc/bloc.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/request/movie_request_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/response/movie_response_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/usecases/movie_usecase.dart';
import 'package:equatable/equatable.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends Bloc<MovieEvent, MovieState> {
  final GetMovieUseCase usecase;
  MovieBloc({required this.usecase}) : super(MovieInitialState());

  @override
  Stream<MovieState> mapEventToState(MovieEvent event) async* {
    if (event is GetMovieEvent) {
      yield MovieLoadingState();
      final result = await usecase(GetMovieRequestEntity(
          reqCode: event.reqCode,
          token: event.token,),);
      yield result.fold((l) => MovieErrorState(error: _mapFailureToMessage(l)),
          (r) => MovieSuccessState(movieResponseEntity: r));
    }
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return "Server Failure";
      default:
        return 'Unexpected Error';
    }
  }
}
