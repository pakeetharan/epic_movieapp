part of 'movie_bloc.dart';

abstract class MovieEvent extends Equatable {
  const MovieEvent();
}

class GetMovieEvent extends MovieEvent {
  final int reqCode;
  final String token;

  const GetMovieEvent(
      {required this.reqCode, required this.token});
  @override
  List<Object?> get props => throw UnimplementedError();
}
