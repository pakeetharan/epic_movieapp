import 'package:epic_movieapp/core/components/loading_view.dart';
import 'package:epic_movieapp/core/constants/constants.dart';
import 'package:epic_movieapp/core/injections/injection_container.dart';
import 'package:epic_movieapp/features/feature_home_page/presentation/bloc/movie_bloc.dart';
import 'package:epic_movieapp/features/feature_home_page/presentation/widgets/movie_list.dart';
// import 'package:epic_movieapp/models/movies.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Home extends StatelessWidget {
  Home({Key? key}) : super(key: key);

  final bloc = sl<MovieBloc>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
          title: const Text(
            "Movie App",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: 'FrScript',
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 36.0,
            ),
          ),
        ),
        body: Stack(children: <Widget>[
          Column(
            children: [
              Image.asset(
                "assets/images/top_bg.png",
                fit: BoxFit.fitWidth,
              ),
              const Spacer()
            ],
          ),
          SingleChildScrollView(
            child: Column(children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height,
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: buildBody(),
              ),
            ]),
          ),
        ]));
  }

  BlocProvider<MovieBloc> buildBody() {
    return BlocProvider(
      create: (_) => bloc,
      child: BlocBuilder<MovieBloc, MovieState>(
        builder: (context, state) {
          if (state is MovieInitialState) {
            _dispatchInit(context);
            return const LoadingView();
          } else if (state is MovieLoadingState) {
            return const LoadingView();
          } else if (state is MovieSuccessState) {
            return ListView.builder(
              itemCount: state.movieResponseEntity.movies.length,
              itemBuilder: (context, i) =>
                  MovieList(index: i, movie: state.movieResponseEntity),
            );
          } else if (state is MovieErrorState) {
            return ErrorWidget(state.error.toString());
          }
          return ErrorWidget("exception");
        },
      ),
    );
  }

  void _dispatchInit(context) {
    BlocProvider.of<MovieBloc>(context).add(
      GetMovieEvent(
        reqCode: ApiCodes.movieCode,
        token: AppConstants.loginToken,
      ),
    );
  }
}
