import 'package:dio/dio.dart';
import 'package:epic_movieapp/core/constants/constants.dart';
import 'package:epic_movieapp/core/error/exception.dart';
import 'package:epic_movieapp/features/feature_home_page/data/models/request/movie_request.dart';
import 'package:epic_movieapp/features/feature_home_page/data/models/response/movie_response.dart';

abstract class RemoteMovieDataSource {
  Future<GetMovieResponse> movieRequest(GetMovieRequest getMovieRequest);
}

class RemoteMovieDataSourceImpl implements RemoteMovieDataSource {
  final Dio _dio = Dio();

  @override
  Future<GetMovieResponse> movieRequest(GetMovieRequest getMovieRequest) async {
    try {
      final response = await _dio.post(baseUrl, data: getMovieRequest.toJson());
      return GetMovieResponse.fromJson(response.data);
    } on ServerException {
      rethrow;
    }
  }
}
