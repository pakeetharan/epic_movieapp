import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/exception.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/core/network/network_info.dart';
import 'package:epic_movieapp/features/feature_home_page/data/data_source/remote_data_source/remote_movie_data_source.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/request/movie_request_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/response/movie_response_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/repositories/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final NetworkInfo networkInfo;
  final RemoteMovieDataSource remoteData;

  MovieRepositoryImpl({required this.networkInfo, required this.remoteData});

  @override
  Future<Either<Failure, GetMovieResponseEntity>>
      getMovieRequestRepository(
          GetMovieRequestEntity getMovieRequestEntity) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(
            await remoteData.movieRequest(getMovieRequestEntity));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
