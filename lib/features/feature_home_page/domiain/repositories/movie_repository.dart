import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/request/movie_request_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/response/movie_response_entity.dart';

abstract class MovieRepository {
  Future<Either<Failure, GetMovieResponseEntity>>
      getMovieRequestRepository(
          GetMovieRequestEntity getMovieRequestEntity);
}
