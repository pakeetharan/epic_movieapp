import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/core/usecase/usecase.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/request/movie_request_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/entities/response/movie_response_entity.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/repositories/movie_repository.dart';

class GetMovieUseCase
    extends UseCase<GetMovieResponseEntity, GetMovieRequestEntity> {
  final MovieRepository repository;

  GetMovieUseCase({required this.repository});
  @override
  Future<Either<Failure, GetMovieResponseEntity>> call(
      GetMovieRequestEntity params) async {
    return await repository.getMovieRequestRepository(params);
  }
}
