import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/features/feature_login/domain/entities/request/login_request.dart';
import 'package:epic_movieapp/features/feature_login/domain/entities/response/login_response.dart';
import 'package:dartz/dartz.dart';

abstract class LoginRepository {
  Future<Either<Failure, GetLoginResponseEntity>> getLoginRequestRepository(
      GetLoginRequestEntity getLoginRequestEntity);
}
