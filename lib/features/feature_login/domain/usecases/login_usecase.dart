import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:epic_movieapp/core/usecase/usecase.dart';
import 'package:epic_movieapp/features/feature_login/domain/entities/request/login_request.dart';
import 'package:epic_movieapp/features/feature_login/domain/entities/response/login_response.dart';
import 'package:epic_movieapp/features/feature_login/domain/repositories/login_repository.dart';

class GetLoginUseCase
    extends UseCase<GetLoginResponseEntity, GetLoginRequestEntity> {
  final LoginRepository repository;

  GetLoginUseCase({required this.repository});
  @override
  Future<Either<Failure, GetLoginResponseEntity>> call(
      GetLoginRequestEntity params) async {
    return await repository.getLoginRequestRepository(params);
  }
}
