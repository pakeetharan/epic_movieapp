import 'package:epic_movieapp/features/feature_login/data/models/request/login_request.dart';

class GetLoginRequestEntity extends GetLoginRequest{
  GetLoginRequestEntity({
    required this.reqCode,
    required this.username,
    required this.password,
  }): super(reqCode: reqCode,username: username, password: password);

  int reqCode;
  String username;
  String password;
}