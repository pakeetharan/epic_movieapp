import 'package:flutter/material.dart';

final TextEditingController usernameController = TextEditingController();
final TextEditingController passwordController = TextEditingController();

Widget usernameField() {
  return TextFormField(
    autofocus: false,
    controller: usernameController,
    keyboardType: TextInputType.emailAddress,
    validator: (value) {
      if (value!.isEmpty) {
        return ("Please Enter Your Username");
      }
      if (!RegExp("^[a-zA-Z0-9+_.-]").hasMatch(value)) {
        return ("Please Enter a Valid Username");
      }
      return null;
    },
    onSaved: (value) {
      usernameController.text = value!;
    },
    textInputAction: TextInputAction.next,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.mail),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Username",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),
  );
}

Widget passwordField() {
  return TextFormField(
    autofocus: false,
    controller: passwordController,
    obscureText: true,
    validator: (value) {
      RegExp regex = RegExp(r'^.{6,20}$');
      if (value!.isEmpty) {
        return ("Password is required for login");
      }
      if (!regex.hasMatch(value)) {
        return ("Enter Valid Password(Min. 6 Character)");
      }
    },
    onSaved: (value) {
      passwordController.text = value!;
    },
    textInputAction: TextInputAction.done,
    decoration: InputDecoration(
      fillColor: Colors.white,
      filled: true,
      errorStyle: const TextStyle(color: Colors.black),
      prefixIcon: const Icon(Icons.vpn_key),
      contentPadding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
      hintText: "Password",
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20),
      ),
    ),);}
