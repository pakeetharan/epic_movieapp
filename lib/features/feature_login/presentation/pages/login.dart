import 'package:epic_movieapp/core/constants/constants.dart';
import 'package:epic_movieapp/core/injections/injection_container.dart';
import 'package:epic_movieapp/features/feature_home_page/presentation/pages/home.dart';
import 'package:epic_movieapp/features/feature_login/presentation/bloc/login_bloc.dart';
import 'package:epic_movieapp/features/feature_login/presentation/widgets/login_form_fields.dart';
import 'package:epic_movieapp/features/feature_register/presentation/pages/registration.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _bloc = sl<LoginBloc>();

  Widget loginButton() {
    return Builder(builder: (context) {
      return Material(
        elevation: 5,
        borderRadius: BorderRadius.circular(30),
        color: Colors.transparent,
        child: MaterialButton(
          padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            if (_formKey.currentState!.validate()) {
              BlocProvider.of<LoginBloc>(context).add(
                GetLoginEvent(
                  reqCode: ApiCodes.loginCode,
                  username: usernameController.text,
                  password: passwordController.text,
                ),
              );
            }
          },
          child: const Text(
            "Login",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      );
    });
  }

  Widget registerButton() {
    return Material(
      elevation: 5,
      borderRadius: BorderRadius.circular(30),
      color: Colors.transparent,
      child: MaterialButton(
          padding: const EdgeInsets.fromLTRB(20, 15, 20, 15),
          minWidth: MediaQuery.of(context).size.width,
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const Registration()));
          },
          child: const Text(
            "Create Account",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold),
          )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: const Text(
          "Movie App",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontFamily: 'FrScript',
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 36.0,
          ),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: BlocProvider(
          create: (context) => _bloc,
          child: Center(
            child: SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: BlocConsumer<LoginBloc, LoginState>(
                  listener: (context, state) {
                    if (state is LoginSuccessState) {
                      if (state.loginResponseEntity.code == 200) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text("Logged in Successfully!"),
                            backgroundColor: Colors.green,
                          ),
                        );
                        AppConstants.loginToken =
                            state.loginResponseEntity.token.toString();
                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (context) => Home(),
                          ),
                        );
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text(state.loginResponseEntity.msg),
                            backgroundColor: Colors.red,
                          ),
                        );
                      }
                    } else if (state is LoginErrorState) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(state.error.toString()),
                          backgroundColor: Colors.red,
                        ),
                      );
                    }
                  },
                  builder: (context, state) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.15,
                            ),
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.black.withOpacity(0.07),
                                  border: Border.all(color: Colors.black12),
                                  borderRadius: const BorderRadius.only(
                                      topLeft: Radius.circular(40.0),
                                      topRight: Radius.circular(40.0),
                                      bottomLeft: Radius.circular(40.0),
                                      bottomRight: Radius.circular(40.0))),
                              child: Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Text(
                                      "Login",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontFamily: 'FrScript',
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 36.0,
                                      ),
                                    ),
                                    const SizedBox(height: 30),
                                    usernameField(),
                                    const SizedBox(height: 25),
                                    passwordField(),
                                    const SizedBox(height: 35),
                                    loginButton(),
                                    SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.05,
                                    ),
                                    Container(
                                      child: state is LoginLoadingState
                                          ? const CircularProgressIndicator()
                                          : null,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            //Spacer(),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.15,
                            ),
                            const Text(
                              "Don't have an account? ",
                              style: TextStyle(color: Colors.white),
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            registerButton(),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
