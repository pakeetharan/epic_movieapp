import 'package:epic_movieapp/core/error/exception.dart';
import 'package:epic_movieapp/core/network/network_info.dart';
import 'package:epic_movieapp/features/feature_login/data/data_sources/remote_login_data_source.dart';
import 'package:epic_movieapp/features/feature_login/domain/entities/response/login_response.dart';
import 'package:epic_movieapp/features/feature_login/domain/entities/request/login_request.dart';
import 'package:epic_movieapp/core/error/failures.dart';
import 'package:dartz/dartz.dart';
import 'package:epic_movieapp/features/feature_login/domain/repositories/login_repository.dart';

class LoginRepositoryImpl implements LoginRepository {
  final NetworkInfo networkInfo;
  final RemoteLoginDataSource remoteData;

  LoginRepositoryImpl({required this.networkInfo, required this.remoteData});

  @override
  Future<Either<Failure, GetLoginResponseEntity>> getLoginRequestRepository(
      GetLoginRequestEntity getLoginRequestEntity) async {
    if (await networkInfo.isConnected) {
      try {
        return Right(await remoteData.loginRequest(getLoginRequestEntity));
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}
