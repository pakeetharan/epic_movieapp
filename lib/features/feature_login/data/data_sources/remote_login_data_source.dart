import 'package:dio/dio.dart';
import 'package:epic_movieapp/core/constants/constants.dart';
import 'package:epic_movieapp/features/feature_login/data/models/request/login_request.dart';
import 'package:epic_movieapp/features/feature_login/data/models/response/login_response.dart';

abstract class RemoteLoginDataSource {
  Future<GetLoginResponse> loginRequest(GetLoginRequest getLoginRequest);
}

class RemoteLoginDataSourceImpl implements RemoteLoginDataSource {
  final Dio _dio = Dio();

  @override
  Future<GetLoginResponse> loginRequest(GetLoginRequest getLoginRequest) async {
    try {
      final response = await _dio.post(baseUrl, data: getLoginRequest.toJson());
      return GetLoginResponse.fromJson(response.data);
    } on Exception {
      rethrow;
    }
  }
}
