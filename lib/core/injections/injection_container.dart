import 'package:connectivity/connectivity.dart';
import 'package:epic_movieapp/core/network/network_info.dart';
import 'package:epic_movieapp/features/feature_home_page/data/data_source/remote_data_source/remote_movie_data_source.dart';
import 'package:epic_movieapp/features/feature_home_page/data/repositories/movie_repository.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/repositories/movie_repository.dart';
import 'package:epic_movieapp/features/feature_home_page/domiain/usecases/movie_usecase.dart';
import 'package:epic_movieapp/features/feature_home_page/presentation/bloc/movie_bloc.dart';
import 'package:epic_movieapp/features/feature_login/data/data_sources/remote_login_data_source.dart';
import 'package:epic_movieapp/features/feature_login/data/repositories/login_repository.dart';
import 'package:epic_movieapp/features/feature_login/domain/repositories/login_repository.dart';
import 'package:epic_movieapp/features/feature_login/domain/usecases/login_usecase.dart';
import 'package:epic_movieapp/features/feature_login/presentation/bloc/login_bloc.dart';
import 'package:epic_movieapp/features/feature_register/data/data_sources/remote_data_source/remote_register_data_source.dart';
import 'package:epic_movieapp/features/feature_register/data/repositories/register_repository.dart';
import 'package:epic_movieapp/features/feature_register/domain/repositories/register_repository.dart';
import 'package:epic_movieapp/features/feature_register/domain/usecases/register_usecase.dart';
import 'package:epic_movieapp/features/feature_register/presentation/bloc/register_bloc.dart';
import 'package:get_it/get_it.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //! Call BLoC
  // Login BLoC
  sl.registerFactory(
    () => LoginBloc(
      usecase: sl(),
    ),
  );
  // Register BLoC
  sl.registerFactory(
    () => RegisterBloc(
      usecase: sl(),
    ),
  );
  // Movie BLoC
  sl.registerFactory(
    () => MovieBloc(
      usecase: sl(),
    ),
  );

  //! Data Source
  // Login Data Source
  sl.registerLazySingleton<RemoteLoginDataSource>(
    () => RemoteLoginDataSourceImpl(),
  );
  // Register Data Source
  sl.registerLazySingleton<RemoteRegisterDataSource>(
    () => RemoteRegisterDataSourceImpl(),
  );
  // Movie Data Source
  sl.registerLazySingleton<RemoteMovieDataSource>(
    () => RemoteMovieDataSourceImpl(),
  );

  //! Repositories
  // Login Repository
  sl.registerLazySingleton<LoginRepository>(
    () => LoginRepositoryImpl(
      networkInfo: sl(),
      remoteData: sl(),
    ),
  );
  // Register Reppository
  sl.registerLazySingleton<RegisterRepository>(
    () => RegisterRepositoryImpl(
      networkInfo: sl(),
      remoteData: sl(),
    ),
  );
  // Movie Repository
  sl.registerLazySingleton<MovieRepository>(
    () => MovieRepositoryImpl(
      networkInfo: sl(),
      remoteData: sl(),
    ),
  );

  //! Usecases
  // Login UseCase
  sl.registerLazySingleton(
    () => GetLoginUseCase(
      repository: sl(),
    ),
  );
  // Register UseCase
  sl.registerLazySingleton(
    () => GetRegisterUseCase(
      repository: sl(),
    ),
  );
  // Movie UseCase
  sl.registerLazySingleton(
    () => GetMovieUseCase(
      repository: sl(),
    ),
  );

  //! Core
  // Network Info
  sl.registerLazySingleton<NetworkInfo>(
    () => NetworkInfoImpl(),
  );
  // Connectivity
  sl.registerLazySingleton(
    () => Connectivity(),
  );
}
