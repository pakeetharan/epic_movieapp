const String baseUrl = 'https://arvinapps.com/movies/api.php';

class AppConstants {
  static String loginToken = "";
}

class ApiCodes {
  static int regCode = 100;
  static int loginCode = 101;
  static int movieCode = 102;
}
