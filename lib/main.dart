import 'package:epic_movieapp/features/feature_login/presentation/pages/login.dart';
// import 'package:epic_movieapp/features/feature_splash_screen/pages/splash_screen.dart';
import 'package:flutter/material.dart';
import './core/injections/injection_container.dart' as ij;

void main() async {
  await ij.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const Login(),
    );
  }
}
